package com.michalrys.htmltables.wwwfilereader;

import com.michalrys.htmltables.tables.Tables;
import com.michalrys.htmltables.tables.WebTables;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WWWFileReader implements TextFileReader {

    private static final String ALLOWABLE_EXTENSION_1 = "htm";
    private static final String ALLOWABLE_EXTENSION_2 = "html";
    private Tables tables = new WebTables();
    private boolean theadOpen = false;
    private boolean tableOpen = false;
    private boolean tableTwiceOpen = false;
    private boolean trOpen = false;
    private boolean tdOpen = false;

    private boolean theadClose = false;
    private boolean tableClose = false;
    private boolean tableTwiceClose = false;
    private boolean trClose = false;
    private boolean tdClose = false;

    private int currentTableId = -1;
    private int currentRowId = -1;
    private int currentColumnId = -1;

    @Override
    public boolean read(File file) {
        String extension = getExtension(file);
        if (!extension.equals(ALLOWABLE_EXTENSION_1) && !extension.equals(ALLOWABLE_EXTENSION_2)) {
            return false;
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                checkLinesForOpenTags(line);
                if (tableTwiceOpen) {
                    return true;
                }
                if (tdOpen == true) {
                    extractToTables(line);
                }
                checkLinesForCloseTags(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private String getExtension(File file) {
        return file.getName().replaceAll(".*\\.", "").toLowerCase();
    }

    @Override
    public Tables getWebTables() {
        return tables;
    }

    private boolean containsTag(String line, String tag) {
        line = line.replaceAll("\\t", "");
        String regex = ".*<" + tag + ">.*|.*<" + tag + " .*>.*|.*<.* " + tag + ">.*|.*<.* " + tag + " .*>.*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(line.toLowerCase());
        return matcher.matches();
    }

    private void checkLinesForOpenTags(String line) {
        if (containsTag(line, "table")) {
            if (tableOpen == true) {
                tableTwiceOpen = true;
            }
            tableOpen = true;
            currentTableId++;
        }

        if (containsTag(line, "thead")) {
            theadOpen = true;
        }

        if (containsTag(line, "tr")) {
            trOpen = true;
            currentRowId++;
        }

        if (containsTag(line, "td")) {
            tdOpen = true;
            currentColumnId++;
        }
    }

    private void checkLinesForCloseTags(String line) {
        if (containsTag(line, "/table")) {
            tableClose = true;
            tableOpen = false;
            currentColumnId = -1;
            currentRowId = -1;
        }

        if (containsTag(line, "/thead")) {
            theadClose = true;
            theadOpen = false;
        }

        if (containsTag(line, "/tr")) {
            trClose = true;
            trOpen = false;
            currentColumnId = -1;
        }

        if (containsTag(line, "/td")) {
            tdClose = true;
            tdOpen = false;
        }
    }

    private void extractToTables(String line) {
        line = line.trim();

        String regex1 = ".*<td>|.*<td [^/>]*>|.*<[^/]* td>|.*<[^/]* td [^/>]*>";
        String regex2 = ".*<TD>|.*<TD [^/>]*>|.*<[^/]* TD>|.*<[^/]* TD [^/>]*>";
        String regex3 = "[\\s]*</td>.*|[\\s]*</td .*>.*|[\\s]*<.* /td>.*|[\\s]*<.* /td .*>.*";
        String regex4 = "[\\s]*</TD>.*|[\\s]*</TD .*>.*|[\\s]*<.* /TD>.*|[\\s]*<.* /TD .*>.*";

        line = line.replaceAll(regex1, "");
        line = line.replaceAll(regex2, "");

        line = line.replaceAll(regex3, "");
        line = line.replaceAll(regex4, "");

        line = line.replaceAll("<sub>", "_");
        line = line.replaceAll("</sub>", "");


        if (theadOpen) {
            tables.inTableAddTHEAD(currentTableId, currentColumnId, line);
        } else {
            tables.inTableAddCell(currentTableId, currentRowId, currentColumnId, line);
        }
    }
}