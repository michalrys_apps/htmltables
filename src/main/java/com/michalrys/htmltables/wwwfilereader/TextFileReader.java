package com.michalrys.htmltables.wwwfilereader;

import com.michalrys.htmltables.tables.Tables;

import java.io.File;

public interface TextFileReader {
    boolean read(File file);

    Tables getWebTables();
}
