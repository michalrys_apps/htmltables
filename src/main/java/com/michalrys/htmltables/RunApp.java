package com.michalrys.htmltables;

import com.michalrys.htmltables.excel.ExcelFile;
import com.michalrys.htmltables.tables.Tables;
import com.michalrys.htmltables.wwwfilereader.TextFileReader;
import com.michalrys.htmltables.wwwfilereader.WWWFileReader;

import java.io.File;
import java.nio.charset.Charset;

public class RunApp {
    public static void main(String[] args) {
        String fileToFirstFile = args[0];
        File file = new File(fileToFirstFile);

//        String exampleFile = "./src/resources/example1.htm";

        TextFileReader reader = new WWWFileReader();
        reader.read(file);
        Tables webTables = reader.getWebTables();

        ExcelFile excelFile = new ExcelFile(webTables, file);
        excelFile.setUpWorkbook();
        excelFile.createTablesInSheets();
        excelFile.writeToFile();
    }
}
