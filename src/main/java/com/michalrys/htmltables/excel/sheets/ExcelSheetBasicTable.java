package com.michalrys.htmltables.excel.sheets;

import com.michalrys.htmltables.excel.ExcelStyles;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelSheetBasicTable implements ExcelSheet {
    private final ExcelStyles excelStyles;
    private final String sheetName;

    private final Workbook workbook;
    private final Sheet sheet;

    public ExcelSheetBasicTable(ExcelStyles excelStyles, String sheetName) {
        this.excelStyles = excelStyles;
        this.sheetName = sheetName;
        workbook = excelStyles.getWorkbook();
        sheet = workbook.createSheet(sheetName);
    }

    @Override
    public void fillCell(int rowId, int columnId, String value, boolean isThead) {
        Row row;
        Cell cell;

        if (sheet.getRow(rowId) == null) {
            row = sheet.createRow(rowId);
        } else {
            row = sheet.getRow(rowId);
        }

        if (row.getCell(columnId) == null) {
            cell = row.createCell(columnId);
        } else {
            cell = row.getCell(columnId);
        }

        cell.setCellValue(value);
        if (isThead && rowId == 0) {
            cell.setCellStyle(excelStyles.getCellStyleHeadCell());
        } else {
            cell.setCellStyle(excelStyles.getCellStyleNormalCell());
        }

        sheet.autoSizeColumn(columnId, true); // FIXME maybe - this is time consuming !
    }
}
