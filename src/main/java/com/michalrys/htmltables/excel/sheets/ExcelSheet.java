package com.michalrys.htmltables.excel.sheets;

public interface ExcelSheet {
    void fillCell(int rowId, int columnId, String value, boolean isThead);
}
