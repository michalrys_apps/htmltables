package com.michalrys.htmltables.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;

import java.awt.Color;

public class ExcelStyles {
    private final Workbook workbook;
    private CellStyle cellStyleHeadCell;
    private CellStyle cellStyleNormalCell;


    public ExcelStyles(Workbook workbook) {
        this.workbook = workbook;
    }

    public void setUpStyles() {
        XSSFFont fontNormal = (XSSFFont) workbook.createFont();
        fontNormal.setFontName("Calibri");
        fontNormal.setFontHeightInPoints((short) 11);

        XSSFFont fontBold = (XSSFFont) workbook.createFont();
        fontBold.setFontName("Calibri");
        fontBold.setFontHeightInPoints((short) 11);
        fontBold.setBold(true);

        // setup styles
        cellStyleHeadCell = workbook.createCellStyle();
        cellStyleHeadCell.setFont(fontBold);
        byte[] colorHeadCellRGB = {(byte) 155, (byte) 155, (byte) 155};
        Color colorHeadCell = new Color(155, 155, 155); //for preview color only
        ((XSSFCellStyle) cellStyleHeadCell).setFillForegroundColor(new XSSFColor(colorHeadCellRGB, new DefaultIndexedColorMap()));
        cellStyleHeadCell.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeadCell.setAlignment(HorizontalAlignment.LEFT);
        cellStyleHeadCell.setBorderBottom(BorderStyle.THIN);
        cellStyleHeadCell.setBorderTop(BorderStyle.THIN);
        cellStyleHeadCell.setBorderLeft(BorderStyle.THIN);
        cellStyleHeadCell.setBorderRight(BorderStyle.THIN);

        this.cellStyleNormalCell = workbook.createCellStyle();
        cellStyleNormalCell.setFont(fontNormal);
        byte[] colorWhite = {(byte) 255, (byte) 255, (byte) 255};
        Color colorWhiteTemp = new Color(255, 255, 255);
        ((XSSFCellStyle) cellStyleNormalCell).setFillForegroundColor(new XSSFColor(colorWhite, new DefaultIndexedColorMap()));
        cellStyleNormalCell.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleNormalCell.setAlignment(HorizontalAlignment.LEFT);
        cellStyleNormalCell.setBorderBottom(BorderStyle.THIN);
        cellStyleNormalCell.setBorderLeft(BorderStyle.THIN);
        cellStyleNormalCell.setBorderRight(BorderStyle.THIN);
        cellStyleNormalCell.setBorderTop(BorderStyle.THIN);
    }

    public Workbook getWorkbook() {
        return workbook;
    }

    public CellStyle getCellStyleHeadCell() {
        return cellStyleHeadCell;
    }

    public CellStyle getCellStyleNormalCell() {
        return cellStyleNormalCell;
    }
}
