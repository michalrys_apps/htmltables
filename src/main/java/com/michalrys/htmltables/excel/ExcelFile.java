package com.michalrys.htmltables.excel;

import com.michalrys.htmltables.excel.sheets.ExcelSheet;
import com.michalrys.htmltables.excel.sheets.ExcelSheetBasicTable;
import com.michalrys.htmltables.tables.TableCords;
import com.michalrys.htmltables.tables.Tables;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExcelFile {
    private final Tables tables;

    private static final String EXCEL_EXTENSION = ".xlsx";
    private static final String POSTFIX = "_TABLES";
    private final String filePath;

    private final Workbook workbook;
    private final ExcelStyles excelStyles;

    public ExcelFile(Tables webTables, File file) {
        this.tables = webTables;
        filePath = convertToExcelFilePath(file.getPath());
        workbook = new XSSFWorkbook();
        excelStyles = new ExcelStyles(workbook);
    }

    public void setUpWorkbook() {
        excelStyles.setUpStyles();
    }

    public void createTablesInSheets() {
        List<TableCords> tablesCords = tables.getTablesCords();
        if (tablesCords.size() == 0) {
            return;
        }

        int previousTableId = tablesCords.get(0).getTableId();
        int currentTableId = previousTableId;
        boolean isThead = tables.isThead(currentTableId);
        int rowId;
        int columnId;

        ExcelSheet sheet = new ExcelSheetBasicTable(excelStyles, String.valueOf(currentTableId));

        for (TableCords cord : tablesCords) {
            System.out.println(cord); //---------------
            currentTableId = cord.getTableId();

            if (previousTableId != currentTableId) {
                sheet = new ExcelSheetBasicTable(excelStyles, String.valueOf(currentTableId));
                isThead = tables.isThead(currentTableId);
                previousTableId = currentTableId;
            }

            rowId = cord.getRowId();
            columnId = cord.getColumnId();
            String value = tables.getFromTableCell(currentTableId, rowId, columnId);

            sheet.fillCell(rowId, columnId, fixValue(value), isThead);
//            sheet.fillCell(rowId, columnId, value, isThead);
        }
    }

    private String fixValue(String value) {
        // special unicode characters:
        // \u00a0  =  NO-BREAK SPACE

        Pattern patternMPa = Pattern.compile("[+-]?[\\s\\u00a0 0-9]*[,]?[\\s\\u00a0 0-9]*[\\s]*(MPa)?[\\s]*(\\(g\\))?");
        Matcher matcherMPa = patternMPa.matcher(value);
        if (matcherMPa.matches()) {
            value = value.replaceAll("MPa", "");
            value = value.replaceAll("\\(g\\)", "");
        }
        //-6 134,6 //
        Pattern patternComaAndSpaces = Pattern.compile("[+-]?[\\s\\u00a0 0-9]*[,]?[\\s\\u00a0 0-9]*");
        Matcher matcherComaAndSpaces = patternComaAndSpaces.matcher(value);

        if (matcherComaAndSpaces.matches()) {
            value = value.replaceAll("\\u00a0","");
            value = value.replaceAll(",", ".");
            value = value.replaceAll(" ","");
        }

        return value;
    }

    public void writeToFile() {
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            workbook.write(fileOutputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            System.out.println(filePath.replaceAll(".*/", "") + " :   There was problem with writing a file.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(filePath.replaceAll(".*/", "") + " :   There was problem with writing a file.");
        }
    }

    private String convertToExcelFilePath(String filePath) {
        String fileName = filePath.replaceAll(".*/", "");
        fileName = filePath.replaceAll(".*\\\\", "");
        String pathOnly = filePath.replaceAll(fileName, "");
        String nameOnly = fileName.replaceAll("\\..*", "");
        String result = pathOnly + nameOnly + POSTFIX + EXCEL_EXTENSION;
        return result;
    }
}