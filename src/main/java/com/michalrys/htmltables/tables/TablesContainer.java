package com.michalrys.htmltables.tables;

import java.util.List;

public interface TablesContainer {

    void inTableAddCell(TableCords tableCords, String value);

    void inTableAddTHEAD(TableCords tableCords, String value);

    String getFromTableCell(TableCords tableCords);

    List<Integer> getTablesIds();

    List<TableCords> getTablesCords();

    boolean isThead(int tableId);
}
