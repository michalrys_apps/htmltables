package com.michalrys.htmltables.tables;

import java.util.List;

public class WebTables implements Tables {
    TablesContainer tables = new TablesContainerImpl();

    @Override
    public void inTableAddTHEAD(int tableId, int theadId, String value) {
        tables.inTableAddTHEAD(new TableCords(tableId, 0, theadId), value);
    }

    @Override
    public void inTableAddCell(int tableId, int rowId, int columnId, String value) {
        tables.inTableAddCell(new TableCords(tableId, rowId, columnId), value);
    }

    @Override
    public String getFromTableCell(int tableId, int rowId, int columnId) {
        return tables.getFromTableCell(new TableCords(tableId, rowId, columnId));
    }

    @Override
    public List<Integer> getTablesIds() {
        return tables.getTablesIds();
    }

    @Override
    public List<TableCords> getTablesCords() {
        return tables.getTablesCords();
    }

    @Override
    public boolean isThead(int tableId) {
        return tables.isThead(tableId);
    }

    @Override
    public String toString() {
        return "Tables: " + tables;
    }
}
