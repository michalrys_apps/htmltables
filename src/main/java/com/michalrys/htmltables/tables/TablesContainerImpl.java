package com.michalrys.htmltables.tables;

import java.util.*;

class TablesContainerImpl implements TablesContainer {
    private Map<TableCords, String> tables = new LinkedHashMap<>();
    private Map<Integer, Boolean> hasTableTHEAD = new LinkedHashMap<>();

    @Override
    public void inTableAddTHEAD(TableCords tableCords, String value) {
        inTableAddCell(tableCords, value);
        hasTableTHEAD.put(tableCords.getTableId(), true);
    }

    @Override
    public void inTableAddCell(TableCords tableCords, String value) {
        if(tables.containsKey(tableCords)) {
            if(value.isEmpty()) {
                return;
            }
            if(!tables.get(tableCords).isEmpty()) {
                value = tables.get(tableCords) + "\n" + value;
            }
        }
        tables.put(tableCords, value);
    }

    @Override
    public String getFromTableCell(TableCords tableCords) {
        return tables.get(tableCords);
    }

    @Override
    public List<Integer> getTablesIds() {
        List<Integer> tablesIds = new ArrayList<>();
        for(TableCords cords : tables.keySet()) {
            tablesIds.add(cords.getTableId());
        }
        return tablesIds;
    }

    @Override
    public List<TableCords> getTablesCords() {
        return new ArrayList<>(tables.keySet());
    }

    @Override
    public boolean isThead(int tableId) {
        return hasTableTHEAD.get(tableId);
    }

    @Override
    public String toString() {
        return tables.toString();
    }
}
