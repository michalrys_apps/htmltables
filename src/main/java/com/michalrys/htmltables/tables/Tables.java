package com.michalrys.htmltables.tables;

import java.util.List;

public interface Tables {

    void inTableAddTHEAD(int tableId, int theadId, String value);

    void inTableAddCell(int tableId, int rowId, int ColumnId, String value);

    String getFromTableCell(int tableId, int rowId, int ColumnId);

    List<Integer> getTablesIds();

    List<TableCords> getTablesCords();

    boolean isThead(int tableId);
}