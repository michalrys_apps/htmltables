package com.michalrys.htmltables.tables;

public class TableCords {
    private final int tableId;
    private final int rowId;
    private final int columnId;

    public TableCords(int tableId, int rowId, int columnId) {
        this.tableId = tableId;
        this.rowId = rowId;
        this.columnId = columnId;
    }

    public int getTableId() {
        return tableId;
    }

    public int getRowId() {
        return rowId;
    }

    public int getColumnId() {
        return columnId;
    }

    @Override
    public String toString() {
        return "[T=" + tableId + ",R=" + rowId + ",C=" + columnId + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableCords that = (TableCords) o;

        if (tableId != that.tableId) return false;
        if (rowId != that.rowId) return false;
        return columnId == that.columnId;
    }

    @Override
    public int hashCode() {
        int result = tableId;
        result = 31 * result + rowId;
        result = 31 * result + columnId;
        return result;
    }
}