package com.michalrys.htmltables.wwwfilereader;

import com.michalrys.htmltables.tables.Tables;
import com.michalrys.htmltables.tables.WebTables;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class WWWFileReaderTest {

    @Test
    public void shouldReadHtmlFile() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example1.html";
        File file = new File(exampleFile);

        // when
        boolean statusOfReadingFile = reader.read(file);

        // then
        Assert.assertTrue(file.exists());
        Assert.assertTrue(statusOfReadingFile);
    }

    @Test
    public void shouldReadHtmFile() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example1.htm";
        File file = new File(exampleFile);

        // when
        boolean statusOfReadingFile = reader.read(file);

        // then
        Assert.assertTrue(file.exists());
        Assert.assertTrue(statusOfReadingFile);
    }

    @Test
    public void shouldNotReadOtherFilesThanHtmlOrHtm() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example1not.nothtml";
        File file = new File(exampleFile);

        // when
        boolean statusOfReadingFile = reader.read(file);

        // then
        Assert.assertTrue(file.exists());
        Assert.assertFalse(statusOfReadingFile);
    }

    @Test
    public void shouldCreateWebTablesObjectsFromHtmlFileOneTagPerLine() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example1simpleSingleTable.htm";
        File file = new File(exampleFile);

        Tables tablesExpected = new WebTables();
        tablesExpected.inTableAddTHEAD(0, 0, "Description");
        tablesExpected.inTableAddTHEAD(0, 1, "Value");
        tablesExpected.inTableAddCell(0, 1, 0, "Static assessment enabled");
        tablesExpected.inTableAddCell(0, 1, 1, "Yes");
        tablesExpected.inTableAddCell(0, 2, 0, "Fatigue assessment enabled");
        tablesExpected.inTableAddCell(0, 2, 1, "Yes");
        tablesExpected.inTableAddCell(0, 3, 0, "Fatigue assessment computation method");
        tablesExpected.inTableAddCell(0, 3, 1, "Fatigue limit");
        tablesExpected.inTableAddCell(0, 4, 0, "Overload case");
        tablesExpected.inTableAddCell(0, 4, 1, "F2: Constant stress ratio");

        String cell00Expected = tablesExpected.getFromTableCell(0, 0, 0);
        String cell01Expected = tablesExpected.getFromTableCell(0, 0, 1);

        reader.read(file);

        // when
        Tables tables = reader.getWebTables();
        String cell00 = tables.getFromTableCell(0, 0, 0);
        String cell01 = tables.getFromTableCell(0, 0, 1);

        // only for preview
        System.out.println(tablesExpected.toString());
        System.out.println("-----------");
        System.out.println(tables.toString());


        // then
        Assert.assertEquals(tablesExpected.toString(), tables.toString());
        Assert.assertEquals(cell00Expected, cell00);
        Assert.assertEquals(cell01Expected, cell01);
    }

    @Test
    public void shouldCreateWebTablesObjectsFromHtmlFileWhereSeveralTagsAreInOneLine() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example6simpleSingleTable.html";
        File file = new File(exampleFile);

        Tables tablesExpected = new WebTables();
        tablesExpected.inTableAddTHEAD(0, 0, "Description");
        tablesExpected.inTableAddTHEAD(0, 1, "Value");
        tablesExpected.inTableAddCell(0, 1, 0, "Fatigue assessment computation method");
        tablesExpected.inTableAddCell(0, 1, 1, "Fatigue limit");
        tablesExpected.inTableAddCell(0, 2, 0, "Overload case");
        tablesExpected.inTableAddCell(0, 2, 1, "F2: Constant stress ratio");

        String cell00Expected = tablesExpected.getFromTableCell(0, 0, 0);
        String cell01Expected = tablesExpected.getFromTableCell(0, 0, 1);

        reader.read(file);

        // when
        Tables tables = reader.getWebTables();
        String cell00 = tables.getFromTableCell(0, 0, 0);
        String cell01 = tables.getFromTableCell(0, 0, 1);

        // only for preview
        System.out.println(tablesExpected.toString());
        System.out.println("-----------");
        System.out.println(tables.toString());


        // then
        Assert.assertEquals(tablesExpected.toString(), tables.toString());
        Assert.assertEquals(cell00Expected, cell00);
        Assert.assertEquals(cell01Expected, cell01);
    }

    @Test
    public void shouldCreateWebTablesObjectsFromHtmlOtherExampleTable() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example4simpleSingleTable.html";
        File file = new File(exampleFile);

        Tables tablesExpected = new WebTables();
        tablesExpected.inTableAddTHEAD(0, 0, "Load Case");
        tablesExpected.inTableAddTHEAD(0, 1, "Name");
        tablesExpected.inTableAddTHEAD(0, 2, "Stress Ratio");
        tablesExpected.inTableAddTHEAD(0, 3, "Scaling Factor");
        tablesExpected.inTableAddCell(0, 1, 0, "2");
        tablesExpected.inTableAddCell(0, 1, 1, "LCASE=20 - Step 2 Substep 2, Time/Freq 2.000000");
        tablesExpected.inTableAddCell(0, 1, 2, "-");
        tablesExpected.inTableAddCell(0, 1, 3, "1,0");
        tablesExpected.inTableAddCell(0, 2, 0, "3");
        tablesExpected.inTableAddCell(0, 2, 1, "LCASE=21 - Step 2 Substep 2, Time/Freq 2.000000");
        tablesExpected.inTableAddCell(0, 2, 2, "-");
        tablesExpected.inTableAddCell(0, 2, 3, "1,0");
        tablesExpected.inTableAddCell(0, 3, 0, "4");
        tablesExpected.inTableAddCell(0, 3, 1, "LCASE=22 - Step 2 Substep 2, Time/Freq 2.000000");
        tablesExpected.inTableAddCell(0, 3, 2, "-");
        tablesExpected.inTableAddCell(0, 3, 3, "1,0");

        String cell00Expected = tablesExpected.getFromTableCell(0, 0, 0);
        String cell01Expected = tablesExpected.getFromTableCell(0, 0, 1);

        reader.read(file);

        // when
        Tables tables = reader.getWebTables();
        String cell00 = tables.getFromTableCell(0, 0, 0);
        String cell01 = tables.getFromTableCell(0, 0, 1);

        // only for preview
        System.out.println(tablesExpected.toString().substring(0, 450));
        System.out.println("-----------");
        System.out.println(tables.toString().substring(0, 450));


        // then
        Assert.assertEquals(tablesExpected.toString().substring(0, 450), tables.toString().substring(0, 450));
//        Assert.assertEquals(cell00Expected, cell00);
//        Assert.assertEquals(cell01Expected, cell01);
    }
    @Test
    public void shouldCreateWebTablesObjectsFromHtmlOtherExampleTableWithEmptyFirstLinesInTHEAD() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example1simpleSingleTable2.htm";
        File file = new File(exampleFile);

        Tables tablesExpected = new WebTables();
        tablesExpected.inTableAddTHEAD(0, 0, "Description");
        tablesExpected.inTableAddTHEAD(0, 1, "Value");
        tablesExpected.inTableAddCell(0, 1, 0, "Static assessment enabled");
        tablesExpected.inTableAddCell(0, 1, 1, "Yes");
        tablesExpected.inTableAddCell(0, 2, 0, "Fatigue assessment enabled");
        tablesExpected.inTableAddCell(0, 2, 1, "Yes");
        tablesExpected.inTableAddCell(0, 3, 0, "Fatigue assessment computation method");
        tablesExpected.inTableAddCell(0, 3, 1, "Fatigue limit");
        tablesExpected.inTableAddCell(0, 4, 0, "Overload case");
        tablesExpected.inTableAddCell(0, 4, 1, "F2: Constant stress ratio");

        String cell00Expected = tablesExpected.getFromTableCell(0, 0, 0);
        String cell01Expected = tablesExpected.getFromTableCell(0, 0, 1);

        reader.read(file);

        // when
        Tables tables = reader.getWebTables();
        String cell00 = tables.getFromTableCell(0, 0, 0);
        String cell01 = tables.getFromTableCell(0, 0, 1);

        // only for preview
//        System.out.println(tablesExpected.toString());
//        System.out.println("-----------");
//        System.out.println(tables.toString());

        // then
        Assert.assertEquals(tablesExpected.toString(), tables.toString());
        Assert.assertEquals(tablesExpected.getFromTableCell(0,0,0), tables.getFromTableCell(0,0,0));
        Assert.assertEquals(tablesExpected.getFromTableCell(0,0,1), tables.getFromTableCell(0,0,1));
    }

    @Test
    public void shouldCreateWebTablesObjectsFromHtmlFileBigFile() {
        // given
        TextFileReader reader = new WWWFileReader();
        String exampleFile = "./src/resources/example1.htm";
        File file = new File(exampleFile);

        Tables tablesExpected = new WebTables();
        tablesExpected.inTableAddTHEAD(33, 0, "Description");
        tablesExpected.inTableAddCell(33, 2, 2, "-1 581 MPa");
        String cell00Expected = tablesExpected.getFromTableCell(33, 0, 0);
        String cell01Expected = tablesExpected.getFromTableCell(33, 2, 2);

        reader.read(file);

        // when
        Tables tables = reader.getWebTables();
        String cell00 = tables.getFromTableCell(0, 0, 0);
        String cell01 = tables.getFromTableCell(33, 2, 2);

        // only for preview
        System.out.println(tablesExpected.toString());
        System.out.println("-----------");
        System.out.println(tables.toString());

        // then
//        Assert.assertEquals(tablesExpected.toString().substring(0,30), tables.toString().substring(0,30));
        Assert.assertEquals(cell00Expected, cell00);
        Assert.assertEquals(cell01Expected, cell01);
    }


}