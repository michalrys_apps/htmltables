package com.michalrys.htmltables.excel;

import com.michalrys.htmltables.tables.Tables;
import com.michalrys.htmltables.wwwfilereader.TextFileReader;
import com.michalrys.htmltables.wwwfilereader.WWWFileReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ExcelFileTest {

    @Test
    public void shouldCreateExcelFileBasedOnWebTableFromReadFileSimpleSingleTable() {
        //given
        String exampleFile = "./src/resources/example1simpleSingleTable.htm";
        File file = new File(exampleFile);

        TextFileReader reader = new WWWFileReader();
        reader.read(file);
        Tables webTables = reader.getWebTables();

        ExcelFile excelFile = new ExcelFile(webTables, file);
        excelFile.setUpWorkbook();
        excelFile.createTablesInSheets();

        //when
        excelFile.writeToFile();

        File fileExcel = new File("./src/resources/example1simpleSingleTable_TABLES.xlsx");

        //then
        Assert.assertTrue(fileExcel.exists());
    }

    @Test
    public void shouldCreateExcelFileBasedOnWebTableFromReadFileBigSeveralTables() {
        //given
        String exampleFile = "./src/resources/example1.htm";
        File file = new File(exampleFile);

        TextFileReader reader = new WWWFileReader();
        reader.read(file);
        Tables webTables = reader.getWebTables();

        ExcelFile excelFile = new ExcelFile(webTables, file);
        excelFile.setUpWorkbook();
        excelFile.createTablesInSheets();

        //when
        excelFile.writeToFile();

        File fileExcel = new File("./src/resources/example1_TABLES.xlsx");

        //then
        Assert.assertTrue(fileExcel.exists());
    }

    @Test
    public void shouldProperlyConvertNumericalTextToNumbersForOneInterestingTable() {
        //given
        String exampleFile = "./src/resources/example1_one_table.htm";
        File file = new File(exampleFile);

        TextFileReader reader = new WWWFileReader();
        reader.read(file);
        Tables webTables = reader.getWebTables();

        ExcelFile excelFile = new ExcelFile(webTables, file);
        excelFile.setUpWorkbook();
        excelFile.createTablesInSheets();

        //when
        excelFile.writeToFile();

        File fileExcel = new File("./src/resources/example1_one_table_TABLES.xlsx");

        //then
        Assert.assertTrue(fileExcel.exists());
    }

    @Test
    public void shouldDeleteTagsProperlyFromTheadTd() {
        //given
        String exampleFile = "./src/resources/example1_one_table_cc.htm";
        File file = new File(exampleFile);

        TextFileReader reader = new WWWFileReader();
        reader.read(file);
        Tables webTables = reader.getWebTables();

        ExcelFile excelFile = new ExcelFile(webTables, file);
        excelFile.setUpWorkbook();
        excelFile.createTablesInSheets();

        //when
        excelFile.writeToFile();

        File fileExcel = new File("./src/resources/example1_one_table_cc_TABLES.xlsx");

        //then
        Assert.assertTrue(fileExcel.exists());
    }
}