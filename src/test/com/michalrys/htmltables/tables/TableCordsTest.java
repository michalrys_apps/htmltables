package com.michalrys.htmltables.tables;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TableCordsTest {

    private static final int TABLE_ID_EXAMPLE_1 = 1;
    private static final int ROW_ID_EXAMPLE_1 = 2;
    private static final int COLUMN_ID_EXAMPLE_1 = 3;
    private TableCords tableCords;

    @Before
    public void setUp() throws Exception {
        tableCords = new TableCords(TABLE_ID_EXAMPLE_1, ROW_ID_EXAMPLE_1, COLUMN_ID_EXAMPLE_1);
    }

    @Test
    public void shouldReturnTableId() {
        //given
        //when
        int tableId = tableCords.getTableId();
        //then
        Assert.assertEquals(TABLE_ID_EXAMPLE_1, tableId);
    }

    @Test
    public void shouldReturnTColumnId() {
        //given
        //when
        int columnId = tableCords.getColumnId();
        //then
        Assert.assertEquals(COLUMN_ID_EXAMPLE_1, columnId);
    }

    @Test
    public void shouldReturnTRowId() {
        //given
        //when
        int rowId = tableCords.getRowId();
        //then
        Assert.assertEquals(ROW_ID_EXAMPLE_1, rowId);
    }

    @Test
    public void shouldReturUsefullStringRepresentation() {
        //given
        String usefullStringRepesentationOfTableCord = "" +
                "[T=" + TABLE_ID_EXAMPLE_1 + ",R=" + ROW_ID_EXAMPLE_1 + ",C=" + COLUMN_ID_EXAMPLE_1 + "]";
        //when
        String toString = tableCords.toString();
        //then
        Assert.assertEquals(usefullStringRepesentationOfTableCord, toString);
    }

    @Test
    public void shouldBeTheSameObjectForCordsWithTheSameCoordinates() {
        //given
        TableCords newCords = new TableCords(TABLE_ID_EXAMPLE_1, ROW_ID_EXAMPLE_1, COLUMN_ID_EXAMPLE_1);
        //when
        //then
        Assert.assertEquals(tableCords, newCords);
    }
}