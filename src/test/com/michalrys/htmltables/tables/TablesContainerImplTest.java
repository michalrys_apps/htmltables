package com.michalrys.htmltables.tables;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TablesContainerImplTest {

    private final TablesContainer tablesContainer = new TablesContainerImpl();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldAddTHEADtoTables() {
        //given
        String testValue = "Test value";
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), testValue);

        //when
        String thead = tablesContainer.getFromTableCell(new TableCords(0, 0, 0));

        //then
        Assert.assertEquals(testValue, thead);
    }

    @Test
    public void shouldAddNullToTHEADTables() {
        //given
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), null);

        //when
        String thead = tablesContainer.getFromTableCell(new TableCords(0, 0, 0));
        System.out.println(thead);

        //then
        Assert.assertEquals(null, thead);
    }

    @Test
    public void shouldAddEmptyStringToTHEADTables() {
        //given
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), "");

        //when
        String thead = tablesContainer.getFromTableCell(new TableCords(0, 0, 0));
        System.out.println(thead);

        //then
        Assert.assertEquals("", thead);
    }

    @Test
    public void shouldNotAddEmptyStringValueForExistingValuesInTHEAD() {
        //given
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), "test");
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), "");

        //when
        String thead = tablesContainer.getFromTableCell(new TableCords(0, 0, 0));
        System.out.println(thead);

        //then
        Assert.assertEquals("test", thead);
    }

    @Test
    public void shouldAddAdditionalValueInNewLineWhenThereIsSomethingInTHEAD() {
        //given
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), "test");
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), "test2");

        //when
        String thead = tablesContainer.getFromTableCell(new TableCords(0, 0, 0));
        System.out.println(thead);

        //then
        Assert.assertEquals("test\ntest2", thead);
    }

    @Test
    public void shouldGiveDescriptiveStringRepresentation() {
        //given
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 0), "Ala");
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 1), "has");
        tablesContainer.inTableAddTHEAD(new TableCords(0, 0, 2), "a cat");
        String expectedDescription = "{[T=0,R=0,C=0]=Ala, [T=0,R=0,C=1]=has, [T=0,R=0,C=2]=a cat}";

        //when
        String toString = tablesContainer.toString();

        //then
        Assert.assertEquals(expectedDescription, toString);
    }



}